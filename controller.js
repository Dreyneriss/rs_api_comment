const commentController = {}

const Pool = require("pg").Pool;
const pool = new Pool({
    user: 'picarrotAdmin',
    host: '51.159.24.37',
    database: 'db_picarrot_commentaire',
    password: 'e}KY8x>1Ad?sC_r:BG@n3',
    port: 36144,
})

commentController.getComments = async (req,res)=> {
    pool.query('SELECT * FROM commentaire', (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
commentController.getComment = async (req,res)=> {
   const id = req.params.id.toString()
   pool.query('SELECT * FROM commentaire WHERE uuid_comment = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
   })
}
commentController.getCommentsFromPost = async (req,res)=> {
    const id = req.params.id.toString()
    pool.query('SELECT * FROM commentaire WHERE uuid_post = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
commentController.getCommentsFromUser = async (req,res)=> {
    const id = req.params.id.toString()
    pool.query('SELECT * FROM commentaire WHERE uuid_user = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
// remarque :
// J'ai mis uid_post et uid_user dans les paramètres d'adresse car, vu que c'était des ints, ils faisaient chier.
//  J'y comprends pas grand chose, franchement...
// Mais du coup, étant donné qu'à terme les uids seront des... uuid ! Eh bien ce seront des strings et
//  ça passera mieux par le body que par l'adresse. Enfin, ça n'est que mon avis.
commentController.createComment = async (req,res)=> {
    const { post, user, comment } = req.body

    let date_ob = new Date();
    let day = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();

    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();
    const date = year + "-" + month + "-" + day + " " + hours+":"+minutes+":"+seconds;

    pool.query('INSERT INTO commentaire(comment_date, uuid_post, uuid_user, comment) VALUES ($1, $2, $3, $4)', [date, post, user, comment], (error, results) => {
        if (error) {
            throw error
        }
        res.status(201).send(`comment added`)
    })
}
// faire passer l'uuid par l'adresse ou par le body ?
commentController.editComment = async (req,res)=> {
    const { id, comment } = req.body

    let date_ob = new Date();
    let day = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();

    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();
    const newDate = year + "-" + month + "-" + day + " " + hours+":"+minutes+":"+seconds;

    pool.query(
        'UPDATE commentaire SET comment = $1, comment_edit_date = $2 WHERE uuid_comment = $3',
        [comment, newDate, id],
        (error, results) => {
            if (error) {
                throw error
            }
            res.status(200).send(`Comment modified with ID: ${id}`)
        }
    )
}
commentController.removeComment = async (req,res)=> {
    const id = req.params.id.toString()

    pool.query('DELETE FROM commentaire WHERE uuid_comment = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).send(`Comment deleted with ID: ${id}`)
    })
}
commentController.removeCommentsFromPost = async (req,res)=> {
    const id = req.params.id.toString()

    pool.query('DELETE FROM commentaire WHERE uuid_post = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).send(`Comment deleted with ID: ${id}`)
    })
}
commentController.removeCommentsFromUser = async (req,res)=> {
    const id = req.params.id.toString()

    pool.query('DELETE FROM commentaire WHERE uuid_user = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).send(`Comment deleted with ID: ${id}`)
    })
}
// add optional param : User and Post
commentController.countComments = async (req,res)=> {
    pool.query('SELECT COUNT(*) FROM commentaire', (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
commentController.countCommentsForUser = async (req,res)=> {
    const userId = req.params.userId.toString()
    pool.query('SELECT COUNT(*) FROM commentaire WHERE uuid_user = $1', [userId], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
commentController.countCommentsForPost = async (req,res)=> {
    const postId = req.params.postId.toString()
    pool.query('SELECT COUNT(*) FROM commentaire WHERE uuid_post = $1', [postId], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
commentController.countCommentsForPostAndUser = async (req,res)=> {
    const userId = req.params.userId.toString()
    const postId = req.params.postId.toString()
    pool.query('SELECT COUNT(*) FROM commentaire WHERE uuid_user = $1 and uuid_post = $2', [userId, postId], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}

module.exports = commentController
